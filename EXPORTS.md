# Exports en ES6 <a name="inicio"/>

## Contenido
+ 1. [Export default](#default)
+ 2. [Export nombrado](#nombrado)
+ 3. [Anterior sintaxis](#Anterior)

---
## Export default <a name="default"/>[↑](#inicio)
Con export default podemos exportar una variable, o función por defecto de un archivo así:

    function hello() {
	    return 'Hello!
    }
    
    export default hello

o  

    export default function hello() {
	    return 'Hello!
    }
    
Para importarla no necesitamos hacerlo con su nombre, podemos usar cualquier nombre y por defecto nos traerá la única variable o función que exportó así:

    import myHello from './module'
    
    console.log(myHello())


---
## Export nombrado <a name="nombrado"/>[↑](#inicio)
A diferencia del export default en esta situación **podemos exportar más de una variable o función** que se encuentre en nuestro archivo, pero para importarlas debemos hacerlo con el nombre exacto de esa variable entre llaves.

Para exportar lo hacemos así:

    export function hello() {
	    return 'Hello!'
    }
    
    export const bye = 'Bye!'``

**Podemos importar solo lo que necesitemos así:**

    import { hello } from './module'
  
    console.log(hello())``

También **podemos importar más de un elemento nombrando cada uno:**

    import { hello, bye } from './module'
    
    console.log(hello())
    console.log(bye)


**Podemos cambiarles los nombres** de la siguiente manera:

    import { hello, bye as byeGreeting } from './module'
    
    console.log(hello())
    console.log(byeGreeting)

Y **podemos importar todas las funciones de una sola vez:**

    import * as allGreetings from './module'
    
    console.log(allGreetings.hello())
    console.log(allGreetings.bye)


---
## Anterior sintaxis <a name="Anterior"/>[↑](#inicio)
Cuando no nos funcione la sintaxis actual debido a no tenerla soportada podemos usar la siguiente:

**Para exportar lo hacemos así:**

    function hello() {
      return 'Hello!'
    }

    module.exports = hello

**Para importar así:**
    const hello = require('./module')
    console.log(hello())

Con esta sintaxis también podemos exportar más de una variable o función pasándolas como un objeto.