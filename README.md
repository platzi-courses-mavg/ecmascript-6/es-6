# ECMAScript <a name="inicio"/>

## Contenido
+ 1. [¿Qué es?](#que-es)
+ 2. [ECMAScript 6](#6)
+ 3. [ECMAScript 7](#7)
+ 4. [ECMAScript 8](#8)
+ 5. [ECMAScript 9](#9)
+ 6. [ECMAScript 10](#10)

---
## ¿Qué es? <a name="que-es"/>[↑](#inicio)
Es la especificación del lenguaje propuesta por *ECMA Institucional*, Institución encargada de los estándares.

**Java Script.** Es el lenguaje de programación que utiliza esta especificación para trabajar sobre estas características que van siendo añadidas año con año a partir del 2015, año donde se lanzó la versión 6. 


---
## ECMAScript 6 <a name="6"/>[↑](#inicio)
*Características*
* Template literals.
* Multiline.
* Spread operator (Operador de Propagación)
* Let y Const.
* Propiedad de Objetos mejorada.
* Arrow Functions.
* Promise.
* Classes.
* Modules.
* Generators. 

[Código y notas](./src/es6.js)

---
## ECMAScript 7 <a name="7"/>[↑](#inicio)
Nace en Junio de 2016.

*Características*
* Icludes.
* Exponent.

[Código y notas](./src/es7.js)

---
## ECMAScript 8 <a name="8"/>[↑](#inicio)
Nace en Junio de 2017 

*Características*
* Object.entries()
* Object.values()
* Pading.
* Trailing commas.
* Async & Await.

[Código y notas](./src/es8.js)

---
## ECMAScript 9 <a name="9"/>[↑](#inicio)
Nace en Junio de 2018 

*Características*
* Opreador de reposo.
* Propiedades de propagación.
* Promisse.finally()
* Mejoras para Regex.

[Código y notas](./src/es9.js)

---
## ECMAScript 10 <a name="10"/>[↑](#inicio)
Nace en Junio de 2019 

*Características*
* flat()
* flatMap()
* trim()
* Optional catch binding.
* Object.fromEntries()
* Objecto de tipo simbolo.

[Código y notas](./src/es10.js)