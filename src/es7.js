/* ===========================================================================
================================ ECMAScript 7 ================================
==============================================================================

Dos principales caracteristicas que se añadieron a la revisión 7 de ES.     

================================== Icludes. ==================================
  Método que trabaja sobre un arreglo o un string, nos permite saber si hay 
  cierto elemento dentro de valor. Viene a sustituir de cierta manera el 
  metodo 'indexOf()' pero de una forma más amigable.
*/
let numbers = [1, 2, 3, 4, 5];

if (numbers.includes(5))
  console.log('Si se encuentra el valor 5');
else 
  console.log('No se encuentra');

/* array.includes(element, index) donde:
element => representa el valor que se va a buscar
index => hace referencia a la posición en la cuál comienza la búsqueda */
const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
console.log("array: ", array);
// console.log("array: ", array.map(n => { return n }));

if (array.includes(10, 3)) 
  console.log("El valor 10 se encuntra a partir de la posición 3")
else
  console.log("El valor 10 NO se encuntra a partir de la posición 3")

if (array.includes(5, 6)) 
  console.log("El valor 5 se encuntra a partir de la posición 6")
else
  console.log("El valor 5 NO se encuntra a partir de la posición 6")

if (array.includes(5, 3)) 
  console.log("El valor 5 se encuntra a partir de la posición 3")
else
  console.log("El valor 5 NO se encuntra a partir de la posición 3")


// ================================ Exponent. ================================
let base = 4;
let exponent = 3;
let result = base ** exponent

console.log(result);