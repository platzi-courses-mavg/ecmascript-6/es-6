function newFunction(name, age, country) {
  var name = name || 'Miguel';
  var age = age ||  '24';
  var country = country || 'MX';

  console.log(name, age, country);
}


/* ===========================================================================
================================ ECMAScript 6 ================================
=========================================================================== */
function newFunction2(name = 'Miguel', age = 24, country = 'MX') {
  console.log(name, age, country);
}

newFunction2();
newFunction2('Ricardo', 23, 'CO')

/* 
  Para correr el código seleccionar el fragmento de código a ejecutar, dar click 
  derecho y elegir la opción 'Run Code', (Se puede también presionando Ctrl + Alt + N). 
  Antes se debe tener instalada la extension 'Code Runner' en VS Code.
*/

// =========================== Template literals. ============================ 
// Permiten separar o unir varios elementos
let hello = "Hello";
let world = "World";
let epicPhrase = hello + ' ' + world; // Sin ES6
console.log(epicPhrase);
let epicPhrase2 = `${hello} ${world}`;
console.log(epicPhrase2);

// ============================== Multiline. ================================
let lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n" + // Sin ES6
            "otra frase épica que necesitamos."

let lorem2 = `Otra frace épica que necesitamos
y aquí hay es otra frase épica.`;

console.log(lorem);
console.log(lorem2);

// Deestructuración
let person = {
  'name': 'Miguel',
  'age': 24,
  'country': 'MX'
}
console.log(person.name, person.age, person.country);

let { name, age, country } = person;
console.log(name, age, country);
console.log(name, age);


// =============== Spread operator (Operador de Propagación) ================= 
// Permite expandir de ciertas situaciones varios elementos
let team1 = ['Miguel', 'Jualian', 'Ricardo'];
let team2 = ['Andrea Michelle', 'Valeria', 'Diana'];

let education = ['David', ...team1, ...team2];

console.log(education);


/* ================================== Let. ===================================
  Es una nueva forma en la cuál podemos trabajar con los elementos que queremos 
  guardar en memoria, en este caso nuestras variables. Con let estos valores solo 
  están disponibles en el scope (bloque de código) en el cuál ha sido definido. 
  Mientras que var permite disponer de sus valores de forma global.
*/

{ var globalVar = 'Global Var'; }

{ 
  let globalLet = 'Global Let';   
  console.log(globalLet);
}

console.log(globalVar);
//console.log(globalLet);


/* ================================= Const. ==================================
  No permite reasignar sus valores, es una constante que nunca cambia su valor y
  al igual que let, su valor solo está disponibles en el scope (bloque de código) 
  en el cuál ha sido definido. 
*/

const a = 'b';
a = 'a';

var b = 'b';
b = 'a';
console.log(b);

const a0 = 'a0';
{ const a1 = 'a1'; }
console.log(a0);
console.log(a1);



// ==================== Propiedad de Objetos mejorada. =======================
let name = 'Miguel';
let age = 24;

obj = { name: name, age: age}; // Sin ES6
obj2 = {name, age}; // ES6
console.log(obj2);


// ============================ Arrow Functions. =============================
const names = [
  { name: 'Miguel', age: 24 },
  { name: 'Miranda', age: 25 },
  { name: 'Héctor', age: 25 }
]

let listOfNames = names.map(function(item) {
  console.log(item.name);
})

let listOfNames2 = names.map(item => connsole.log(item.name)); // ES6

const listOfNames3 = (name, age, country) => {
  // Bloque de código
}

const listOfNames4 = name => {
  // Bloque de código
}

const square = num => num * num;
console.log(square(5));


/* =============================== Promises. =================================
  Nos permiten trabajar el asíncronismo, JavaScript no es un lenguaje 
  sincronico, es decir, permite ejecutar muchos elementos al mismo tiempo
  en lugar de ejecutar elemento por elemento.
  
  Javascript utiliza un modelo asíncrono y no bloqueante, con un loop de 
  eventos implementado con un único thread para sus interfaces de entrada/salida.
  MAS INFO:
  (https://lemoncode.net/lemoncode-blog/2018/1/29/javascript-asincrono)
*/
const helloPromise = () => {
  return new Promise((resolve, reject) => {
    // if (true) {
    if (false) {
      resolve('Hey!');
    } else {
      reject('Ups!!');
    }
  });
}

helloPromise()
  .then(response => console.log(response))
  .then(() => console.log('Hola'))
  .catch(error => console.log(error));


/* ================================ Classes. ================================
  Permiten manejar una sintaxis más clara para el uso de objetos y herencia
  permirtiendo así aplicar la Programación Orienta Objetos (POO)
*/
class calculator {
  /* Contructor. Meétodo para incializar nuestra clase, donde asignamos las
  variables que estarán disponibles en un scope global */
  constructor() {
    this.valueA = 0;
    this.valueB = 0;
  }

  sum(valueA, valueB) {
    this.valueA = valueA;
    this.valueB = valueB;
    return (this.valueA + this.valueB)
  }
}

const calc = new calculator();
console.log(calc.sum(2, 2));


// ================================ Modules. ================================
import hi from './hello';

const hello = hi();
console.log(hello);


/* ============================== Generators. ===============================
  Función especial que retorna una serie de valores según el algoritmo 
  definido. Se utiliza para trabajar con lógicas o una serie de algoritmos 
  y ver como podemos ir ejecutandolos.
  Su función '.next()' actúa de manera similar a los middlewares de Node.
*/
function* helloWorld() { // function* => Sintaxis de un Generator
  if (true) {
    yield 'Hello, '; // yield => Permite retornar algo, y guardar su estado de forma interna
  }
  if (true) {
    yield 'world!';
  }
};

const generatorHello = helloWorld();
console.log(generatorHello.next().value);
console.log(generatorHello.next().value);
console.log(generatorHello.next().value);
