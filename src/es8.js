/* ===========================================================================
================================ ECMAScript 8 ================================
==============================================================================

============================== Object.entries(). =============================
  Método que nos permite devolver la clave y los valores de una matriz.
  Transforma elementos en matriz. Al utilizar esta herramientas disponemos de
  otros métodos como '.length'
*/
const data = {
  frontend: 'Daniel',
  backend: 'Miguel',
  design: 'César', // Trailing comma
}

const entries = Object.entries(data);
console.log(entries);
console.log(entries.length);


/* ============================= Object.values(). ============================
  Método que devuelve solo los valores de un arreglo o un objeto. Al utilizar 
  esta herramienta disponemos de otros métodos como '.length'
*/
const data = {
  frontend: 'Daniel',
  backend: 'Miguel',
  design: 'César', // Trailing comma
}

const values = Object.values(data);
console.log(values);
console.log(values.length);


/* ================================== Pading. ================================
  Permite anteponer o agregar una cadena vacia o elementos a un string.
*/
const string = 'hello';
console.log(string.padStart(8, 'hi '));
console.log(string.padStart(6, 'hi '));
console.log(string.padEnd(10, '...'));
console.log(string.padEnd(6, '...'));
console.log(string.padEnd(11, ' -----'));
console.log(string.padEnd(20, ' -----'));

console.log('food'.padEnd(11, '  -----'));


/* ============================= Trailing commas. ============================
  Consiste en agregar una coma al final del último elemento de un objeto y así
  establecer una forma más amigable y simple para empezar a añadir más 
  elementos sin caer en un error que era muy común. Establece que después del
  último elemento puede haber un valor o no.
*/
const obj = {
  name: 'Miguel',
}


/* ============================== Async & Await. =============================
  INFO:
  https://platzi.com/tutoriales/1789-asincronismo-js/5063-las-promesas-y-async-await-logicamente-no-son-iguales-y-te-explico-el-porque/
*/
const helloWorld = () => {
  return new Promise((resolve, reject) => {
    (false) // if ternario
      ? setTimeout(() => resolve('Hello World!'), 3000)
      : reject(new Error('Test Error'))
  })
};

const helloAsync = async () => {
  const hello = await helloWorld();
  console.log(hello);;
}

helloAsync();

const anotherFunction = async () => {
  try {
    const hello = await helloWorld();
    console.log(hello);
  } catch (error) {
    console.log(error);
  }
}

anotherFunction();