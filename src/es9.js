/* ===========================================================================
================================ ECMAScript 9 ================================
==============================================================================

============================= Opreador de reposo. ============================
  Permite extraer las propiedades de un objeto que aún no se ha construido.
*/
const obj = {
  name: 'Miguel',
  age: '24',
  country: 'MX', // Trailing comma
}

// let { name, ...all } = obj;
// console.log(name, all);

let { country, ...all } = obj;
console.log(all);
let { age, ...all1 } = obj;
console.log(all1);


/* ======================== Propiedades de propagación. ======================
  Nos permiten unir uno, dos, tres o cuantos elementos queramos de objetos a 
  un objeto.
*/
const obj = {
  name: 'Miguel',
  age: '24'
}

const obj1 = {
  ...obj, 
  country: 'MX', // Trailing comma
}

console.log("obj", obj);
console.log("obj1", obj1);


/* ============================ Promisse.finally() ===========================
  Nos permiten saber cuando a terminado el llamado de la promesa y así poder 
  ejecutar alguna función o lógica de código.
  Funciona de manera similar al Finally de un Try / Catch de Java
*/
const helloWorld = () => {
  return new Promise((resolve, reject) => {
    // (true)
    (false)
      // ? resolve('Hello World')
      ? setTimeout(() => resolve('Hello World'), 2000)
      : reject(new Error('Test Error'))
  });
};

helloWorld()
  .then(response => console.log(response))
  .catch(error => console.log(error))
  .finally(() => console.log('Finalizo'))


/* ============================ Mejoras para Regex ===========================
  Permite trabajar los regex como un grupo de elementos, algo así como si fuera
  un array.
*/
const regexData = /([0-9]{4})-([0-9]{2})-([0-9]{2})/
const match = regexData.exec('2020-10-31');
const year = match[1];
const month = match[2];
const day = match[3];

console.log(year, month, day);