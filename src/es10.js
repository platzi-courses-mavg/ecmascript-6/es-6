/* ===========================================================================
================================ ECMAScript 10 ===============================
==============================================================================

=================================== flat(). ==================================
  Recibe como argumento la profundidad a la que se llamará al array.
*/
let array = [1, 2, 3, [1, 2, 3 , [1, 2, 3]]];

console.log(array.flat());
console.log(array.flat(2));


/* ================================ flatMap(). ===============================
  Permite mapear cada elemento, después pasarle una función y aplanarlo según 
  el resultado
*/
let array = [1, 2, 3, 4, 5];

console.log(array.flatMap(value => [value, value * 2]));


/* ================================= trim(). =================================
  Permite eliminar los espacios en blanco de un String
*/
let hello = '                      hello world   ';
console.log(hello);
console.log(hello.trimStart());
console.log(hello.trimEnd());
console.log(hello.trim());


/* ========================= Optional catch binding. =========================
  Permite pasar de forma opcional el parametro de error al valor de catch
*/
try {
  
} catch { // } catch (error) { 
  error
}


/* ========================= Object.fromEntries(). ============================
  Transforma 'clave: valor' a un objeto. Muy útil cuando se trabaja con arreglos
  que se quieren pasar a un objeto o viceversa.
*/
let entries = [["name", "oscar"], ["age", 24]];

console.log(Object.fromEntries(entries));


/* ======================== Objecto de tipo simbolo. ==========================
  Permite acceder a la descripción de un objeto de tipo símbolo.
*/
let mySymbol = 'My Symbol';
let symbol = Symbol(mySymbol);
console.log(symbol.description);